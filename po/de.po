# SOME DESCRIPTIVE TITLE.
# Copyright (C) 2001 Free Software Foundation, Inc.
# Jeff Tranter <tranter@pobox.com>, 2001.
#
msgid ""
msgstr ""
"Project-Id-Version: eject 2.0.13deb-4\n"
"POT-Creation-Date: 2004-05-07 01:11+0200\n"
"PO-Revision-Date: 2004-05-07 01:35+0200\n"
"Last-Translator: nordi <nordi@addcom.de>\n"
"Language-Team:\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=iso-8859-15\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../eject.c:125
#, c-format
msgid ""
"Eject version %s by Jeff Tranter (tranter@pobox.com)\n"
"Usage:\n"
"  eject -h\t\t\t\t-- display command usage and exit\n"
"  eject -V\t\t\t\t-- display program version and exit\n"
"  eject [-vnrsfq] [<name>]\t\t-- eject device\n"
"  eject [-vn] -d\t\t\t-- display default device\n"
"  eject [-vn] -a on|off|1|0 [<name>]\t-- turn auto-eject feature on or off\n"
"  eject [-vn] -c <slot> [<name>]\t-- switch discs on a CD-ROM changer\n"
"  eject [-vn] -t [<name>]\t\t-- close tray\n"
"  eject [-vn] -x <speed> [<name>]\t-- set CD-ROM max speed\n"
"Options:\n"
"  -v\t-- enable verbose output\n"
"  -n\t-- don't eject, just show device found\n"
"  -r\t-- eject CD-ROM\n"
"  -s\t-- eject SCSI device\n"
"  -f\t-- eject floppy\n"
"  -q\t-- eject tape\n"
"  -p\t-- use /proc/mounts instead of /etc/mtab\n"
"  -m\t-- do not unmount device even if it is mounted\n"
msgstr ""
"Eject version %s von Jeff Tranter (tranter@pobox.com)\n"
"Usage:\n"
"  eject -h\t\t\t\t-- gibt die Hilfe aus und beendet das Programm\n"
"  eject -V\t\t\t\t-- gibt Versioninformation aus und beendet das Programm\n"
"  eject [-vnrsfq] [<name>]\t\t-- Laufwerk �ffnen\n"
"  eject [-vn] -d\t\t\t-- zeige Standardlaufwerk an\n"
"  eject [-vn] -a on|off|1|0 [<name>]\t-- auto-eject an-/ausschalten\n"
"  eject [-vn] -c <slot> [<name>]\t-- wechselt CD im CD-Wechsler\n"
"  eject [-vn] -t [<name>]\t\t-- Laufwerk schlie�en\n"
"  eject [-vn] -x <speed> [<name>]\t-- maximale CD-ROM Geschwindigkeit "
"setzen\n"
"Options:\n"
"  -v\t-- zeige Details an\n"
"  -n\t-- Laufwerk nicht �ffnen, nur gefundenes Ger�t anzeigen\n"
"  -r\t-- CD-ROM auswerfen\n"
"  -s\t-- Disk im SCSI Ger�t auswerfen\n"
"  -f\t-- Floppy auswerfen\n"
"  -q\t-- Band auswerfen\n"
"  -p\t-- benutze/proc/mounts statt /etc/mtab\n"
"  -m\t-- Ger�t nicht unmounten, selbst wenn es gemounted ist\n"

#: ../eject.c:148
msgid ""
"Long options:\n"
"  -h --help   -v --verbose\t -d --default\n"
"  -a --auto   -c --changerslot  -t --trayclose  -x --cdspeed\n"
"  -r --cdrom  -s --scsi\t -f --floppy\n"
"  -q --tape   -n --noop\t -V --version\n"
"  -p --proc   -m --no-unmount\n"
msgstr ""
"Lange Optionen:\n"
"  -h --help   -v --verbose\t -d --default\n"
"  -a --auto   -c --changerslot  -t --trayclose  -x --cdspeed\n"
"  -r --cdrom  -s --scsi\t -f --floppy\n"
"  -q --tape   -n --noop\t -V --version\n"
"  -p --proc   -m --no-unmount\n"

#: ../eject.c:156
#, c-format
msgid ""
"Parameter <name> can be a device file or a mount point.\n"
"If omitted, name defaults to `%s'.\n"
"By default tries -r, -s, -f, and -q in order until success.\n"
msgstr ""
"Parameter <Name> kann eine Ger�tedatei oder ein Mount Punkt sein.\n"
"Wenn ausgelassen wird `%s' gew�hlt.\n"
"Versucht standardm��ig -r, -s, -f und -q in dieser Reihenfolge bis es funktioniert.\n"

#: ../eject.c:209
#, c-format
msgid "%s: invalid argument to --auto/-a option\n"
msgstr "%s: ung�ltiges Argument f�r die --auto/-a Option\n"

#: ../eject.c:221
#, c-format
msgid "%s: invalid argument to --changerslot/-c option\n"
msgstr "%s: ung�ltiges Argument f�r die --changerslot/-c Option\n"

#: ../eject.c:233
#, c-format
msgid "%s: invalid argument to --cdspeed/-x option\n"
msgstr "%s: ung�ltiges Argument f�r die --cdspeed/-x Option\n"

#: ../eject.c:273
#, c-format
msgid "eject version %s by Jeff Tranter (tranter@pobox.com)\n"
msgstr "eject Version %s von Jeff Tranter (tranter@pobox.com)\n"

#: ../eject.c:283
#, c-format
msgid "%s: too many arguments\n"
msgstr "%s: zu viele Optionen angegeben\n"

#: ../eject.c:331
#, c-format
msgid "%s: could not allocate memory\n"
msgstr "%s: Konnte keinen Speicher allozieren\n"

#: ../eject.c:404
#, c-format
msgid "%s: CD-ROM auto-eject command failed: %s\n"
msgstr "%s: CD-ROM auto-eject fehlgeschlagen: %s\n"

#: ../eject.c:421
#, c-format
msgid "%s: CD-ROM select disc command failed: %s\n"
msgstr "%s: CD-ROM select disc fehlgeschlagen: %s\n"

#: ../eject.c:427
#, c-format
msgid "%s: CD-ROM load from slot command failed: %s\n"
msgstr "%s: CD-ROM load from slot fehlgeschlagen: %s\n"

#: ../eject.c:431
#, c-format
msgid "%s: IDE/ATAPI CD-ROM changer not supported by this kernel\n"
msgstr "%s: IDE/ATAPI CD-ROM Wechsler von diesem Kernel nicht unterst�tzt\n"

#: ../eject.c:446
#, c-format
msgid "%s: CD-ROM tray close command failed: %s\n"
msgstr "%s: CD-ROM Schlie�en fehlgeschlagen: %s\n"

#: ../eject.c:450
#, c-format
msgid "%s: CD-ROM tray close command not supported by this kernel\n"
msgstr "%s: CD-ROM Schlie�en von diesem Kernel nicht unterst�tzt\n"

#: ../eject.c:467
#, c-format
msgid "%s: CD-ROM select speed command failed: %s\n"
msgstr "%s: CD-ROM selecter vitesse pas s'accomplir: %s\n"

#: ../eject.c:471
#, c-format
msgid "%s: CD-ROM select speed command not supported by this kernel\n"
msgstr ""
"%s: CD-ROM Geschwindigkeit setzen von diesem Kernel nicht unterst�tzt\n"

#: ../eject.c:581
#, c-format
msgid "%s: unable to exec /bin/umount of `%s': %s\n"
msgstr "%s: kann /bin/umount f�r `%s' nicht ausf�hren: %s\n"

#: ../eject.c:586
#, c-format
msgid "%s: unable to fork: %s\n"
msgstr "%s: kann nicht forken: %s\n"

#: ../eject.c:591
#, c-format
msgid "%s: unmount of `%s' did not exit normally\n"
msgstr "%s: Unmounten von `%s' nicht normal beendet\n"

#: ../eject.c:595
#, c-format
msgid "%s: unmount of `%s' failed\n"
msgstr "%s: Unmounten von `%s' fehlgeschlagen\n"

#: ../eject.c:608
#, c-format
msgid "%s: unable to open `%s'\n"
msgstr "%s: kann `%s' nicht �ffnen\n"

#: ../eject.c:653 ../eject.c:740
#, c-format
msgid "unable to open %s: %s\n"
msgstr "kann %s nicht �ffnen: %s\n"

#: ../eject.c:700
#, c-format
msgid "%s: unable to open /etc/fstab: %s\n"
msgstr "%s: kann /etc/fstab nicht �ffnen: %s\n"

#: ../eject.c:750 ../eject.c:984
#, c-format
msgid "%s: unmounting `%s'\n"
msgstr "%s: Unmounte `%s'\n"

#: ../eject.c:823
#, c-format
msgid "%s: `%s' is a multipartition device\n"
msgstr "%s: `%s' ist ein Ger�t mit mehreren Partitionen\n"

#: ../eject.c:828
#, c-format
msgid "%s: `%s' is not a multipartition device\n"
msgstr "%s: `%s' ist kein Ger�t mit mehreren Partitionen\n"

#: ../eject.c:841
#, c-format
msgid "%s: setting CD-ROM speed to auto\n"
msgstr "%s: setze CD-ROM Geschwindigkeit auf auto\n"

#: ../eject.c:843
#, c-format
msgid "%s: setting CD-ROM speed to %dX\n"
msgstr "%s: setze CD-ROM Geschwindigkeit auf %dX\n"

#: ../eject.c:879
#, c-format
msgid "%s: default device: `%s'\n"
msgstr "%s: Standardger�t: `%s'\n"

#: ../eject.c:887
#, c-format
msgid "%s: using default device `%s'\n"
msgstr "%s: benutze Standardger�t `%s'\n"

#: ../eject.c:896
#, c-format
msgid "%s: device name is `%s'\n"
msgstr "%s: Ger�tename ist `%s'\n"

#: ../eject.c:901
#, c-format
msgid "%s: unable to find or open device for: `%s'\n"
msgstr "%s: kann Ger�t `%s' nicht finden/�ffnen\n"

#: ../eject.c:905
#, c-format
msgid "%s: expanded name is `%s'\n"
msgstr "%s: erweiterter Name ist `%s'\n"

#: ../eject.c:910
#, c-format
msgid "%s: `%s' is a link to `%s'\n"
msgstr "%s: `%s' ist ein Link auf `%s'\n"

#: ../eject.c:919
#, c-format
msgid "%s: maximum symbolic link depth exceeded: `%s'\n"
msgstr "%s: maximale symbolische Link Tiefe �berschritten: `%s'\n"

#: ../eject.c:927
#, c-format
msgid "%s: `%s' is mounted at `%s'\n"
msgstr "%s: `%s' ist nach `%s' gemountet\n"

#: ../eject.c:929
#, c-format
msgid "%s: `%s' is not mounted\n"
msgstr "%s: `%s' ist nicht gemountet\n"

#: ../eject.c:941
#, c-format
msgid "%s: `%s' can be mounted at `%s'\n"
msgstr "%s: `%s' kann nach`%s' gemountet werden\n"

#: ../eject.c:943
#, c-format
msgid "%s: `%s' is not a mount point\n"
msgstr "%s: `%s' ist kein mount point\n"

#: ../eject.c:949
#, c-format
msgid "%s: device is `%s'\n"
msgstr "%s: Ger�t ist `%s'\n"

#: ../eject.c:951
#, c-format
msgid "%s: exiting due to -n/--noop option\n"
msgstr "%s: beende wegen -n/--noop Option\n"

#: ../eject.c:959
#, c-format
msgid "%s: enabling auto-eject mode for `%s'\n"
msgstr "%s: aktiviere Auto-Auswurf f�r `%s'\n"

#: ../eject.c:961
#, c-format
msgid "%s: disabling auto-eject mode for `%s'\n"
msgstr "%s: deaktiviere Auto-Auswurf f�r `%s'\n"

#: ../eject.c:971
#, c-format
msgid "%s: closing tray\n"
msgstr "%s: schlie�e jetzt\n"

#: ../eject.c:997
#, c-format
msgid "%s: selecting CD-ROM disc #%d\n"
msgstr "%s: w�hle CD #%d\n"

#: ../eject.c:1015
#, c-format
msgid "%s: trying to eject `%s' using CD-ROM eject command\n"
msgstr "%s: Versuche `%s' mit dem CD eject Befehl auszuwerfen\n"

#: ../eject.c:1019
#, c-format
msgid "%s: CD-ROM eject command succeeded\n"
msgstr "%s: CD eject war erfolgreich\n"

#: ../eject.c:1021
#, c-format
msgid "%s: CD-ROM eject command failed\n"
msgstr "%s: CD eject war nicht erfolgreich\n"

#: ../eject.c:1027
#, c-format
msgid "%s: trying to eject `%s' using SCSI commands\n"
msgstr "%s: versuche `%s' mit SCSI Befehlen auszuwerfen\n"

#: ../eject.c:1031
#, c-format
msgid "%s: SCSI eject succeeded\n"
msgstr "%s: SCSI eject war erfolgreich\n"

#: ../eject.c:1033
#, c-format
msgid "%s: SCSI eject failed\n"
msgstr "%s: SCSI eject war nicht erfolgreich\n"

#: ../eject.c:1039
#, c-format
msgid "%s: trying to eject `%s' using floppy eject command\n"
msgstr "%s: versuche `%s' mit Floppy Befehlen auszuwerfen\n"

#: ../eject.c:1043
#, c-format
msgid "%s: floppy eject command succeeded\n"
msgstr "%s: Floppy eject war erfolgreich\n"

#: ../eject.c:1045
#, c-format
msgid "%s: floppy eject command failed\n"
msgstr "%s: Floppy eject war nicht erfolgreich\n"

#: ../eject.c:1051
#, c-format
msgid "%s: trying to eject `%s' using tape offline command\n"
msgstr "%s: versuche `%s' mit dem Band offline Befehl auszuwerfen\n"

#: ../eject.c:1055
#, c-format
msgid "%s: tape offline command succeeded\n"
msgstr "%s: Band offline war erfolgreich\n"

#: ../eject.c:1057
#, c-format
msgid "%s: tape offline command failed\n"
msgstr "%s: Band offline war nicht erfolgreich\n"

#: ../eject.c:1062
#, c-format
msgid "%s: unable to eject, last error: %s\n"
msgstr "%s: Kann nicht auswerfen! Letzter Fehler: %s\n"

#: ../volname.c:58
msgid "usage: volname [<device-file>]\n"
msgstr "Benutzung: volname [<Ger�tedatei>]\n"

#: ../volname.c:64 ../volname.c:70 ../volname.c:76
msgid "volname"
msgstr "Volname"
